vim.api.nvim_create_autocmd("FileType", {
  pattern = { "java" },
  callback = function()
    vim.api.nvim_set_option_value("colorcolumn", "100", {
      scope = "local",
    })
  end
})

vim.api.nvim_create_autocmd("FileType", {
  pattern = { "rust" },
  callback = function()
    vim.api.nvim_set_option_value("colorcolumn", "98", {
      scope = "local",
    })
  end
})

vim.opt.clipboard:append { "unnamedplus" }

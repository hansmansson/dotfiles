require("lazy").setup({
  {
    "AstroNvim/AstroNvim",
    version = "^4", -- Remove version tracking to elect for nighly AstroNvim
    import = "astronvim.plugins",
    opts = {
      icons_enabled = not os.getenv("DISABLE_ICONS"),
    }
  },
  { import = "plugins" },
})

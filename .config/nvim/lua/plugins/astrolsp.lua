---@type AstroLSPConfig
return {
  "AstroNvim/astrolsp",
  opts = {
    formatting = {
      format_on_save = {
        enabled = false,
      },
    },
  },
}

---@type LazySpec
return {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    options = {
      opt = {
        relativenumber = false,
        colorcolumn = "80",
        guicursor = "i:block",
        wildmode = "longest:full,full",
        list = true,
        listchars = {
          tab = "  ",
          trail = "·",
        },
        foldcolumn = "0",
      },
    },
    diagnostics = {
      virtual_text = false,
      underline = false,
    },
    mappings = {
      i = {
        ["<M-k>"] = {
          "<cmd>lua vim.lsp.buf.signature_help()<cr>",
          desc = "Show LSP signature help.",
        },
      },
    },
  },
}

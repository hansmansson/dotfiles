return {
  { "lukas-reineke/indent-blankline.nvim", enabled = false },
  { "goolord/alpha-nvim", enabled = false },
  { "max397574/better-escape.nvim", enabled = false },
  { "windwp/nvim-ts-autotag", enabled = false },
  { "windwp/nvim-autopairs", enabled = false },
}

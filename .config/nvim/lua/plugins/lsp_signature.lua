return {
  "ray-x/lsp_signature.nvim",
  opts = {
    floating_window = false,
    hint_enable = false,
    max_height = 12,
    max_width = 80,
    --toggle_key = "<M-k>",
  }
}

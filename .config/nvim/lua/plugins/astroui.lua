---@type LazySpec
return {
  "AstroNvim/astroui",
  ---@type AstroUIOpts
  opts = {
    highlights = {
      init = function()
        local search = require("astroui").get_hlgroup("Search")

        return {
          LspSignatureActiveParameter = search,
          LspReferenceRead = search,
          LspReferenceText = search,
          LspReferenceWrite = search,
          MatchParen = require("astrocore").extend_tbl(search, { bold = true }),
          MatchWord = search,
        }
      end,
    },
  },
}

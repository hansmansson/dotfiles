---@type LazySpec
return {
  "nvim-treesitter/nvim-treesitter",
  dependencies = {
    {
      "andymass/vim-matchup",
      init = function()
        vim.g.matchup_matchparen_offscreen = {
          method = "status_manual"
        }
        vim.g.matchup_matchparen_deferred = 1
      end
    },
  },
  opts = function(_, opts)
    opts = require("astrocore").extend_tbl(opts, {
      indent = { enable = true },
      matchup = {
        enable = true,
        disable_virtual_text = true,
      },
    })
    opts.ensure_installed = require("astrocore").list_insert_unique(opts.ensure_installed, {
      "lua",
      "vim",
      "c",
      "lua",
      "rust",
      "python",
      "java",
      "cpp",
      "bash",
      "blueprint",
      "make",
      "json",
      "yaml",
      "diff",
      "markdown"
    })
  end,
}

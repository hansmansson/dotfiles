# Configure status bar and UI style
set -g @color-bg "#282a36"
set -g @color-white "#e4e4e4"
set -g @color-black "#1e222a"
set -g @color-gray "#304452"
set -g @color-blue "#61afef"

set -g status "on"
set -g status-justify "centre"
set -g status-style "bg=#{@color-bg}"

set -g status-left-length "100"
set -g status-left "#[fg=#{@color-black},bg=#{@color-white}] #S "

set -g status-right-length "100"
set -g status-right "#[fg=#{@color-black},bg=#{@color-white}] #h "

setw -g window-status-separator ""
setw -g window-status-style "fg=#{@color-white},bg=#{@color-blue}"
setw -g window-status-current-style bold

setw -g window-status-current-format "#[fg=#{@color-black},bg=#{@color-blue}] #I "
setw -ga window-status-current-format "#[fg=#{@color-white},bg=#{@color-gray}] #W "

setw -g window-status-format "#[fg=#{@color-black},bg=#{@color-white}] #I "
setw -ga window-status-format "#[fg=#{@color-white},bg=#{@color-bg}] #W "

set -g message-command-style "fg=#{@color-black},bg=#{@color-blue}"
set -g message-style "fg=#{@color-black},bg=#{@color-blue}"

set -g pane-active-border-style "fg=#{@color-blue}"
set -g pane-border-style "fg=#{@color-white}"

set -g mode-style "fg=#{@color-black},bg=#{@color-blue}"

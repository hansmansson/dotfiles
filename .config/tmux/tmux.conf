unbind C-b
set -g prefix C-Space
set -g mouse off
bind a last-window
bind A switch-client -l
bind Space copy-mode
bind y paste-buffer

bind / copy-mode \; send /
bind ? copy-mode \; send ?

if-shell "! test -v XDG_CONFIG_HOME" { XDG_CONFIG_HOME="$HOME/.config" }
if-shell "! test -v XDG_DATA_HOME" { XDG_DATA_HOME="$HOME/.local/share" }

set history-file "${XDG_DATA_HOME}/tmux/tmux_history"

set -g base-index 1
set-window-option -g mode-keys vi

set-option -ga terminal-overrides ",xterm-256color:Tc"

source-file -q "${XDG_CONFIG_HOME}/tmux/config.d/*.tmux"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'abhinav/tmux-fastcopy'

# Configure fastcopy
set -g @fastcopy-key F
set -g @fastcopy-shift-action "${XDG_CONFIG_HOME}/tmux/paste.sh {}"
set -g @fastcopy-regex-path "(?:[\\w\\-\\.]+|~)?(?:/[\\w\\-\\.\\+@_]+){1,}\\b"

# Enable continuum
set -g @continuum-restore 'on'

set-environment -g -F TMUX_PLUGIN_MANAGER_PATH "${XDG_DATA_HOME}/tmux/plugins"

if-shell "test ! -d ${XDG_DATA_HOME}/tmux/plugins/tpm" {
   run "git clone https://github.com/tmux-plugins/tpm ${XDG_DATA_HOME}/tmux/plugins/tpm"
   run "${XDG_DATA_HOME}/tmux/plugins/tpm/bin/install_plugins"
}

run '${XDG_DATA_HOME}/tmux/plugins/tpm/tpm'

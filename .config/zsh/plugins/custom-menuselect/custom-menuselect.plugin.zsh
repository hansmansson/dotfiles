zmodload zsh/complist

key_info[Tab]="$terminfo[ht]"

bindkey -M menuselect "$key_info[Control]H" vi-backward-char
bindkey -M menuselect "$key_info[Control]K" vi-up-line-or-history
bindkey -M menuselect "$key_info[Control]L" vi-forward-char
bindkey -M menuselect "$key_info[Control]J" vi-down-line-or-history

bindkey -M menuselect '/' history-incremental-search-forward
bindkey -M menuselect '?' history-incremental-search-backward
bindkey -M menuselect "$key_info[Escape]" vi-insert

bindkey -M menuselect "$key_info[Tab]" vi-down-line-or-history
bindkey -M menuselect "$key_info[BackTab]" vi-up-line-or-history

zstyle ':completion:*:*:*:*:*' menu select

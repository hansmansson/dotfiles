# Editor
export EDITOR="vim"
export VISUAL="vim"
export PAGER="less"

# Set default less options.
export LESS="-g -i -M -R -z-4 -F"

# Set the less input preprocessor.
if [[ -z "$LESSOPEN" ]] && (( $#commands[(i)lesspipe(|.sh)] )); then
  export LESSOPEN="| /usr/bin/env $commands[(i)lesspipe(|.sh)] %s 2>&-"
fi

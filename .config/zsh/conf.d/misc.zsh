# Set general options.
typeset -ga zopts_general=(
  NO_BEEP                   # Be quiet!
  NO_HIST_BEEP              # Be quiet!
  COMBINING_CHARS           # Combine 0-len chars with base chars (eg: accents).
  INTERACTIVE_COMMENTS      # Enable comments in interactive shell.
)


# Set job options.
typeset -ga zopts_job=(
  LONG_LIST_JOBS            # List jobs in the long format by default.
  AUTO_RESUME               # Attempt to resume existing job before creating a new process.
)

setopt $zopts_general \
  $zopts_job


# Bind some keys
bindkey -- "$key_info[PageUp]" up-line-or-history
bindkey -- "$key_info[PageDown]" down-line-or-history
[[ -n "$terminfo[kLFT3]" ]] && bindkey -- "$terminfo[kLFT3]" backward-word
[[ -n "$terminfo[kRIT3]" ]] && bindkey -- "$terminfo[kRIT3]" forward-word


# Do not insert tabs
zstyle ':completion:*' insert-tab false


function tweak-highlight-styles {
  typeset -A FAST_HIGHLIGHT_STYLES
  ZSH_HIGHLIGHT_STYLES[comment]="fg=#5c6370"
  ZSH_HIGHLIGHT_STYLES[path]="fg=white"
  ZSH_HIGHLIGHT_STYLES[path-to-dir]="fg=white"
  export FAST_HIGHLIGHT_STYLES
}


# Load custom completions
fpath=($ZDOTDIR/completions(/N) $fpath)


# Override some stuff
function restore-accept-line() {
  zle .accept-line
}
zle -N accept-line restore-accept-line

function update-cursor-style {
  printf '\e[2 q'
}
zle -N update-cursor-style

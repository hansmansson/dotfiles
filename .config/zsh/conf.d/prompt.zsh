[[ $COLORTERM = *(24bit|truecolor)* ]] || zmodload zsh/nearcolor
autoload -Uz promptinit && promptinit
setopt PROMPT_PERCENT
setopt PROMPT_SUBST

prompt_homebrew_setup() {
  PS1="%F{#98c379}%n%f@%F{#56b6c2}%m%f:%F{#61afef}%~%f %(?.%F{#98c379}❯.%F{#e06c75}❯)%f "
}
prompt_themes+=( homebrew )

prompt homebrew

# Shell
- zsh

# Tools
- git
- fzf
- fd
- ripgrep
- tmux
- vim
- nvim
- ranger
- bottom
- atool
- lesspipe
- ncdu/gdu

# Email
- aerc
- isync/mbsync
- msmtp

## Optional bling
- bat
- exa
- git-delta

# Window manager
- sway
- swayidle
- swayimg
- swaylock
- swaynag
- waybar
- wofi
- kanshi
- grim
- slurp

## Terminal
- kitty
- alacritty

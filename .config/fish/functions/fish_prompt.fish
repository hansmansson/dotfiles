function fish_prompt --description 'Write out the prompt'
    set -l cstatus $status
    set -l normal (set_color normal)
    set -l status_color (set_color green)
    set -l suffix '❯'

    if test $cstatus -ne 0
        set status_color (set_color red)
    end

    echo -n -s (set_color green)(whoami)$normal@(set_color cyan)(prompt_hostname)$normal:
    echo -n -s (set_color blue)(prompt_pwd)
    echo -n -s $status_color " " $suffix " " $normal
end

if not status --is-interactive
    exit
end
set fish_prompt_pwd_dir_length 0
set -g fish_key_bindings fish_hybrid_key_bindings
set fish_greeting

if ! test -d "$XDG_DATA_HOME/fish/fisher"
    git clone https://github.com/jorgebucaran/fisher "$XDG_DATA_HOME/fish/fisher"
end

# User
set -x XDG_CACHE_HOME $HOME/.cache
set -x XDG_CONFIG_HOME $HOME/.config
set -x XDG_DATA_HOME $HOME/.local/share
set -x XDG_STATE_HOME $HOME/.local/state

set -x PAGER less
set -x EDITOR vim
set -x VISUAL vim

# Set default less options.
set -x LESS "-g -i -M -R -z-4 -F"

# Set the less input preprocessor.
if test -z "$LESSOPEN"
  if test -e (command -v lesspipe.sh)
    set -x LESSOPEN "|" (command -v lesspipe.sh) " %s"
  else if test -e (command -v lesspipe)
    set -x LESSOPEN "|" (command -v lesspipe) " %s"
  end
end

set -e LESS_TERMCAP_so

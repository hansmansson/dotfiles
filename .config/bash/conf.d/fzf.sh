if [[ -r /usr/share/fzf/key-bindings.bash ]]; then
  . /usr/share/fzf/key-bindings.bash
fi
if [[ -r /usr/share/fzf/completion.bash ]]; then
  . /usr/share/fzf/completion.bash
fi

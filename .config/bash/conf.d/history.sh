shopt -s histappend
if ! [[ -d $XDG_DATA_HOME/bash/history ]]; then
  mkdir -p "$XDG_DATA_HOME/bash"
fi
HISTFILE=$XDG_DATA_HOME/bash/history
HISTSIZE=1000
HISTFILESIZE=2000
HISTCONTROL=ignoreboth:erasedups
